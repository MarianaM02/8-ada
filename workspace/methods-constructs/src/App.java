import ar.com.ada.oop.instantiation.Person;
import ar.com.ada.oop.overloading.Arithmetic;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {

        Person per1 = new Person();
        Person per2 = new Person("Mariana");
        Person per3 = new Person("Erika", "Farias");

        System.out.println(per1.getName() + " " + per1.getLastName());
        System.out.println(per2.getName() + " " + per2.getLastName());
        System.out.println(per3.getName() + " " + per3.getLastName());

        System.out.println("***************************");

        Arithmetic arit = new Arithmetic();
        Integer num = new Integer(7);

        Integer resultado1 = arit.sum(3,6);
        Scanner sc = new Scanner(System.in);

        System.out.println("Resultado 1 = " + resultado1);
        System.out.println("Resultado 2 = " + arit.sum(num,6));
        System.out.println("Resultado 3 = " + arit.sum(5, new Integer(7)));
        System.out.println("Ingrese un double:");
        System.out.println("Resultado 4 = " +
                arit.sum(new Double(8.2), new Double(sc.nextDouble())));
    }
}
