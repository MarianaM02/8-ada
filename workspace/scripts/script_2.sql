create database PhoneBookDB;

create table Contact (
	id INT auto_increment,
	name VARCHAR(50) not NULL,
	last_name VARCHAR(50) not NULL,
	address VARCHAR(100),
	number_phone VARCHAR(20) not null,
	primary KEY(id)
	);

show tables;

describe contact; 

insert into contact (name, last_name, address, number_phone) 
	values ('luisa', 'naval', 'sarmiento 232', '254791649');
	
insert into contact (name, last_name, address, number_phone) 
	values ('lucia', 'labal', 'anchona 245', '275778668');
	
insert into contact (name, last_name, address, number_phone) 
	values ('andrea', 'alba', 'sarmiento 1567', '362647888');
	
insert into contact (name, last_name, address, number_phone) 
	values ('flor', 'labal', 'corrientes 232', '153467586');
	
insert into contact (name, last_name, address, number_phone) 
	values ('alvaro', 'teruel', 'gomez 232', '125246576');

insert into contact (name, last_name, address, number_phone) 
	values ('luisa', 'naval', 'sarmiento 232', '254791649'),
	('lucia', 'labal', 'anchona 245', '275778668'),
	('andrea', 'alba', 'sarmiento 1567', '362647888'),
	('flor', 'labal', 'corrientes 232', '153467586'),
	('alvaro', 'teruel', 'gomez 232', '125246576');
	
select * from contact;
