create database bot_java_coach;

CREATE TABLE Person (
    personId INT AUTO_INCREMENT,
    name VARCHAR(20),
    PRIMARY KEY(personId)
);

insert into person (name) values ('Luisa');

insert into person (name) values ('Maria'), ('Elena'), ('Fernanda');

select * from person;

select name from person;
select personId from person;

