import java.util.Scanner;

import ar.com.ada.oop.overloading.Arithmetic;

public class App {
	
	public static void main(String[] args) {
	
		Arithmetic sumas = new Arithmetic();
		Integer numerito = new Integer(5);
		
		System.out.println("El resultado 1 es " + sumas.sum(5, 8));
		
		System.out.println("El resultado 2 es " + sumas.sum(4.2, 5.7));
		
		System.out.println("El resultado 3 es " + sumas.sum(3, numerito));
		
		System.out.println("El resultado 4 es " + sumas.sum(new Double(2.2), 5.4));
		
		
		Scanner entrada = new Scanner(System.in);
		System.out.println("Ingrese 2 nros: ");
		
		System.out.println("El resultado 5 es " + sumas.sum(entrada.nextDouble(), new Double(entrada.nextDouble())));
		
	}
}
