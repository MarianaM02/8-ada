package genericas;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Crear tres objetos mediante polimorfismo; un tipo Set, un tipo List, un tipo
 * Map, sin utilizar genéricos. Añadir objetos String a cada colección y mapa.
 * Obtener 1 valor de cada colección. Crear método para imprimir cada colección.
 * 
 */
public class Ejercicio {
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) {

		//////////////////////////////////////////////////////
		System.out.println("\nImpresión de Set");
		Set st = new HashSet();
		st.add("Hola");
		st.add("Mundis");
		st.add("en");
		st.add("Set");

		Iterator itr = st.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}

		//////////////////////////////////////////////////////
		System.out.println("\nImpresión de List");
		List ls = new ArrayList();
		ls.add("Hola");
		ls.add("Mundis");
		ls.add("en");
		ls.add("List");

		itr = ls.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}

		//////////////////////////////////////////////////////
		System.out.println("\nImpresión de Map");
		Map mp = new HashMap();
		mp.put(1, "Hola");
		mp.put(2, "Mundis");
		mp.put(3, "en");
		mp.put(4, "Map");

		// Iterator<Entry<Integer,String>>
		itr = mp.entrySet().iterator();
		while (itr.hasNext()) {
			Map.Entry entry = (Entry) itr.next();
			System.out.println(entry);
		}

		//////////////////////////////////////////////////////
		mostrarCollection(st);
		mostrarCollection(ls);
		mostrarCollection(mp.entrySet());

	}

	/**
	 * Muestra una collection Iterable indicando el tipo y cada entrada en una nueva
	 * linea
	 * 
	 * @param collection
	 */
	@SuppressWarnings("rawtypes")
	public static void mostrarCollection(Collection collection) {
		System.out.println("\nMostrando " + collection.getClass().getSimpleName());
		Iterator itr = collection.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}
	}
}
