package genericas;

public class ClaseGenerica<N extends Number> {
	private N objeto;

	public ClaseGenerica(N objeto) {
		this.objeto = objeto;
	}

	public void obtenerTipo() {
		System.out.println("El tipo T es: " + objeto.getClass().getSimpleName());
	}

	public static void main(String[] args) {

		ClaseGenerica<Integer> cg1 = new ClaseGenerica<>(1);
		// ClaseGenerica<String> cg2 = new ClaseGenerica<>("Un String");

		cg1.obtenerTipo();
		// cg2.obtenerTipo();

	}
}
