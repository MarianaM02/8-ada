package ar.ada.org.local;

import java.util.Arrays;

public class Ticket {
    private Producto[] listaProd;
    private String metodoPago;

    public Ticket(Producto[] listaProd, String metodoPago) {
        this.listaProd = listaProd;
        this.metodoPago = metodoPago;
    }

    public boolean aplicaDescuentoJuguete() {
        int contador = 0;
        int sum = 0;
        for (Producto p : listaProd) {
            if (p instanceof Juguete) {
                contador++;
                sum += p.getPrecioLista();
            }

        }
        return contador > 3 || sum > 3000;

    }

    public boolean pagaConTarjeta() {
        return metodoPago.equalsIgnoreCase("tdc");
    }

    public double calcularTotalConDesc() {

        boolean t = pagaConTarjeta();
        boolean j = aplicaDescuentoJuguete();
        double sum = 0;
        for (Producto p : listaProd) {
            sum += p.aplicarDescuento(t, j);
        }
        return sum;
    }

    public double calcularTotalSinDesc() {

        double sum = 0;
        for (Producto p : listaProd) {
            sum += p.getPrecioLista();
        }
        return sum;
    }

    @Override
    public String toString() {
        double sinDescuento = calcularTotalSinDesc();
        double totalApagar = calcularTotalConDesc();
        return "\"Te sacamos un ojo\"" +
                "\n-------------- "
                + Arrays.toString(listaProd) +
                "\n-------------- " +
                "\nCantidad de productos= " + listaProd.length +
                "\nMetodo Pago= " + metodoPago +
                "\nTotal sin descuento $" + sinDescuento +
                "\nTotal $" + totalApagar;
    }
}
