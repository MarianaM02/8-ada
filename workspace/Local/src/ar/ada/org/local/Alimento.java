package ar.ada.org.local;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Alimento extends Producto {

    public Alimento(String nombre,double precioLista) {
        super(nombre,precioLista);
    }

    @Override
    public double aplicarDescuento(boolean hayDescHerramienta,boolean hayDescJuguete) {
        Calendar hoy = new GregorianCalendar();
        int dia = hoy.get(Calendar.DAY_OF_WEEK);
        // Descuento martes (3) y jueves (5)
        if (dia == 3 || dia == 5){
            return this.getPrecioLista() * 0.9;
        } else {
            return this.getPrecioLista();
        }
    }

}
