package ar.ada.org.local;

public abstract class Producto {

    private String nombre;
    private double precioLista;


    protected Producto(String nombre, double precioLista) {
        this.precioLista = precioLista;
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public double getPrecioLista() {
        return precioLista;
    }

    public abstract double aplicarDescuento(boolean hayDescHerramienta, boolean hayDescJuguete);

    @Override
    public String toString() {
        return "\nNombre Producto: " + nombre + " Precio de Lista: $" + precioLista;

    }
}
