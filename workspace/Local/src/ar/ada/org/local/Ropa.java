package ar.ada.org.local;

public class Ropa extends Producto {
    private String tipo;

    public Ropa(String nombre,double precioLista, String tipo) {
        super(nombre,precioLista);
        this.tipo = tipo;
    }

    @Override
    public double aplicarDescuento(boolean hayDescHerramienta,boolean hayDescJuguete) {
        if (tipo.equals("mujer") || tipo.equals("niño")){
            return this.getPrecioLista() * 0.85;
        } else {
            return this.getPrecioLista();
        }
    }

    @Override
    public String toString() {
        return super.toString() + " Tipo: "+ tipo;

    }
}
