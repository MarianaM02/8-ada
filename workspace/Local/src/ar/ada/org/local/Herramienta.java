package ar.ada.org.local;

public class Herramienta extends Producto {

    public Herramienta(String nombre,double precioLista) {
        super(nombre,precioLista);
    }

    @Override
    public double aplicarDescuento(boolean hayDescHerramienta,boolean hayDescJuguete) {
        if(hayDescHerramienta){
            return this.getPrecioLista()* 0.95;
        }else {
            return this.getPrecioLista();
        }
    }
}
