package ar.ada.org.local;

public class Juguete extends Producto {

    public Juguete(String nombre,double precioLista) {
        super(nombre,precioLista);
    }

    @Override
    public double aplicarDescuento(boolean hayDescHerramienta,boolean hayDescJuguete) {


        if (hayDescJuguete) {
            return this.getPrecioLista() * 0.75;
        }else {
            return this.getPrecioLista();
        }

    }


}
