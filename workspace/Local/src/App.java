import ar.ada.org.local.*;

public class App {
    public static void main(String[] args) {
        Alimento arroz = new Alimento("arroz", 22.50);
        Herramienta martillo = new Herramienta("martillo", 500.48);
        Juguete pelota = new Juguete("pelota", 100.00);
        Ropa pollera = new Ropa("pollera", 800.35, "mujer");
        Ticket ticket = new Ticket(new Producto[]{arroz, martillo, pelota, pollera}, "TDC");
        System.out.println(ticket);

    }
}
