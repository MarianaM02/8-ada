import inheritance.Base;
import inheritance.Derived;

public class Tester {
    public static void main(String[] args) {
        Base b = new Base();
        Derived d = new Derived();

        System.out.println(b.bPublic + b.bProtected + b.bPrivate + d.dPublic + d.dPrivate);
    }
}
