package inheritance;

public class Base {
    public int bPublic;
    protected int bProtected;
    private int bPrivate;

    public static void main(String[] args) {
        Base b = new Base();
        Derived d = new Derived();

        System.out.println(b.bPublic + b.bProtected + b.bPrivate + d.dPublic + d.dPrivate);
    }
}
