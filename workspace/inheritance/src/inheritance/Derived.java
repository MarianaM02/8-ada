package inheritance;

public class Derived extends Base{
    public int dPublic;
    private int dPrivate;

    public static void main(String[] args) {
        Base b = new Base();
        Derived d = new Derived();

        System.out.println(b.bPublic + b.bProtected + b.bPrivate + d.dPublic + d.dPrivate);
    }
}
