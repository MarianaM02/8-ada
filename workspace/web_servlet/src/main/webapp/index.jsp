<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="style.css">
    <title>JSP - Hello World</title>
</head>
<body>
<h1><%= "Hello World from Index!" %>
</h1>
<br/>
<nav>
    <ul>
        <li><a href="index.jsp">Inicio</a></li>
        <li><a href="GaleriaServlet">Galería</a></li>
        <li><a href="AcercaServlet">Acerca</a></li>
        <li><a href="ContactanosServlet">Contáctenos</a></li>
        <li><a href="hello-servlet">Hello Servlet</a></li>
    </ul>
</nav>
</body>
</html>