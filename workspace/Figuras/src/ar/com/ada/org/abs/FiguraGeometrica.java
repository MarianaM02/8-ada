package ar.com.ada.org.abs;

public abstract class FiguraGeometrica {

    public abstract double calcularArea();

}
