package tescasamosunojo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ShoppingCart {

    public static String dayOfWeek;
    public static Integer quantityToys = 0;
    public static Double priceToys;
    private ArrayList<Product> products = new ArrayList<>();

    public ShoppingCart() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        this.dayOfWeek = dateFormat.format(date);
    }

    public ArrayList<Product> getProducts () {return products;}

    public Integer getQuantityToys(ArrayList<Product> products) {
        for (Product product : products) {
            if (product instanceof Toy) {
                quantityToys++;
                priceToys = quantityToys + ((Toy) product).getPrice();
            }
        }
        return quantityToys;
    }
}
