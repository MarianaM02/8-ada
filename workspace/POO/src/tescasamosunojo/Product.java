package tescasamosunojo;

public abstract class Product {

    public abstract Double listPrice ();

    public abstract Double discount ();

}
