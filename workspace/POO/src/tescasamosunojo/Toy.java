package tescasamosunojo;

public class Toy extends Product {

    protected Double price;
    protected String productName;

    @Override
    public Double listPrice() {
        switch (productName) {
            case "Autito":
                price = 2100.00;
                setPrice(price);
                break;
            case "Muñeca":
                price = 1500.50;
                setPrice(price);
                break;
            case "Avión":
                price = 2350.00;
                setPrice(price);
            default:
                System.out.println("Este producto no existe.");
        }
        return price;
    }

    @Override
    public Double discount() {
        double discount;
        double priceDiscToy;
        if (ShoppingCart.quantityToys > 3 || ShoppingCart.priceToys > 3000) {
            discount = price * 0.25;
            priceDiscToy = price - discount;
            setPrice(priceDiscToy);
        }
        return price;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
