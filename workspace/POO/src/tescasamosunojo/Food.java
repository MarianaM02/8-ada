package tescasamosunojo;

public class Food extends Product {

    protected Double price;
    protected String productName;

    public Food() {
    }

    public Food(Double price, String productName) {
        this.price = price;
        this.productName = productName;
    }

    public Double listPrice() {
        switch (productName) {
            case "Arroz":
                price = 1000.00;
                break;
            case "Atún":
                price = 500.50;
                break;
            case "Dulce leche":
                price = 250.00;
            default:
                System.out.println("Este producto no existe.");
        }
        return price;
    }

    public Double discount() {
        double discount;
        double priceDisc = 0;
        if (ShoppingCart.dayOfWeek.equals("Tuesday") || ShoppingCart.dayOfWeek.equals("Thursday")) {
            discount = price * 0.10;
            priceDisc = price - discount;
            setPrice(price);
        }
        return price;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
