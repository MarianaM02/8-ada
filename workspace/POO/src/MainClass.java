public class MainClass {
    public static void main(String[] args) {
        Animal dog = new Animal();
        Animal cat = new Animal();
        Animal dog2 = new Animal();
        Animal cat2 = new Animal();

        dog.setName("Firulais");
        dog.setColor("Marron");
        cat.setName("Pelusa");
        cat.setColor("Blanco");
        dog2.setName("Mailo");
        dog2.setColor("Marron");
        cat2.setName("Firulais");
        cat2.setColor("Marron");

        System.out.println(dog.toString());
        System.out.println(cat.toString());
        System.out.println(dog2.toString());
        System.out.println(cat2.toString());
        System.out.println("Los animales son iguales: " + dog.equals(cat));
        System.out.println("Los animales son iguales: " + dog.equals(dog2));
        System.out.println("Los animales son iguales: " + dog2.equals(cat2));
        System.out.println("Los animales son iguales: " + cat.equals(cat2));

    }

}
