import java.util.Objects;

public class Animal {
    public String breed;
    public String color;
    public String name;

    public boolean move(int distanceX, int distanceY) {
        return true;
    }

    public String sleep() {
        return "Estoy durmiendo";
    }

    public String eat() {
        return "Estoy comiendo";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String toString() {
        return "Animal { name = " + name + ", color = " + color + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        Animal that = (Animal) o;
        return name.equals(that.name) && color.equals(that.color) && breed.equals(that.breed);
    }

    @Override
    public int hashCode() {
        return Objects.hash(breed, color, name);
    }
}