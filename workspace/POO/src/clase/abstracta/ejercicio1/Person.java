package clase.abstracta.ejercicio1;

import java.util.Objects;

public class Person {

    public String name;
    public String lastName;

    public Person () {}

    public Person (String name){
        this.name = name;
    }

    public Person (String name, String lastName){
        this.name = name;
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String toString() {
        return "Animal { Nombre = " + name + ", Apellido = " + lastName + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        Person that = (Person) o;
        return name.equals(that.name) && lastName.equals(that.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, lastName);
    }
}
