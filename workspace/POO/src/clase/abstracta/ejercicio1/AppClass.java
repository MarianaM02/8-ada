package clase.abstracta.ejercicio1;

public class AppClass {
    public static void main(String[] args) {
        Person mother = new Person("Fabiana", "Lanzo");
        Person father = new Person("Pablo", "Rojas");
        Person son = new Person("Pablo", "Rojas");
        Person daughter = new Person("Juana", "Lanzo");

        System.out.println("mother.toString():" + mother.toString());
        System.out.println("father.toString():" + father.toString());
        System.out.println("son.toString():" + son.toString());
        System.out.println("daughter.toString():" + daughter.toString());

        System.out.println("mother.hashCode():" + mother.hashCode());
        System.out.println("father.hashCode():" + father.hashCode());
        System.out.println("son.hashCode():" + son.hashCode());
        System.out.println("daughter.hashCode():" + daughter.hashCode());

        System.out.println("mother.equals(father):" + mother.equals(father));
        System.out.println("mother.equals(son):" + mother.equals(son));
        System.out.println("mother.equals(daughter):" + mother.equals(son));
        System.out.println("father.equals(son):" + mother.equals(son));
        System.out.println("father.equals(daughter):" + mother.equals(son));
        System.out.println("son.equals(daughter):" + mother.equals(son));

    }
}
