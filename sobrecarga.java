public class Parent {
    public Parent(int x){
        System.out.println("A");
    }
}

public class Child extends Parent{
    public Child(int x){
        System.out.println("B");
    }

    public Child(){
        this(123);
        System.out.println("C");
    }

    public static void main(String[] args) {
        new Child();
    }
}
